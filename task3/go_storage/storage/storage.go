package storage

import "task3/go_models/user"

type Storage interface {
	AddUser(user user.User) error
	GetUser(id int) (user.User, error)
	SaveUser(user user.User) error
}
