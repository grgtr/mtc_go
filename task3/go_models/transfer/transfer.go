package transfer

type Transfer struct {
	SenderId   int     `json:"senderId,omitempty"`
	AcceptorId int     `json:"recipientId,omitempty"`
	Amount     float64 `json:"amount"`
}
