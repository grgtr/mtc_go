package handlers

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"task3/go_models/transfer"
	"task3/server_internal/companion"
	"task3/server_internal/config"
)

type Handlers struct {
	Config    *config.Config
	companion *companion.Companion
}

func Create(config *config.Config, companion *companion.Companion) *Handlers {
	return &Handlers{
		Config:    config,
		companion: companion,
	}
}

func (h *Handlers) PostAdd(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		hint, err := json.Marshal(errors.New("only POST method allowed"))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		w.Write(hint)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		hint, _ := json.Marshal(errors.New("incorrect body"))
		w.Write(hint)
		return
	}
	err = r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	transfer := transfer.Transfer{
		SenderId:   -1,
		AcceptorId: -1,
		Amount:     -1,
	}
	err = json.Unmarshal(body, &transfer)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		hint, _ := json.Marshal(errors.New("incorrect json"))
		w.Write(hint)
		return
	}

	transfer.SenderId = -1

	err = h.companion.Transfer(r.Context(), transfer)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		hint, _ := json.Marshal(err)
		w.Write(hint)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (h *Handlers) PostTransfer(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		hint, _ := json.Marshal(errors.New("incorrect body"))
		w.Write(hint)
		return
	}
	err = r.Body.Close()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	transfer := transfer.Transfer{
		SenderId:   -1,
		AcceptorId: -1,
		Amount:     -1,
	}
	err = json.Unmarshal(body, &transfer)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		hint, _ := json.Marshal(errors.New("incorrect json"))
		w.Write(hint)
		return
	}

	// Бизнес операция
	err = h.companion.Transfer(r.Context(), transfer)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		hint, _ := json.Marshal(err)
		w.Write(hint)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (h *Handlers) GetUserInfo(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	usr, err := h.companion.GetUser(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		hint, _ := json.Marshal(err)
		w.Write(hint)
		return
	}

	res, _ := json.Marshal(usr)
	w.Write(res)
	w.WriteHeader(http.StatusOK)
}
