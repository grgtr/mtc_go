package server_app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os/signal"
	"syscall"
	"task3/server_internal/companion"
	"task3/server_internal/config"
	"task3/server_internal/handlers"
	"time"
)

type App struct {
	Config *config.Config
	Server *http.Server
}

func Create(config *config.Config, companion *companion.Companion) *App {
	return &App{
		Config: config,
		Server: &http.Server{
			Addr:    config.Http,
			Handler: HandlerBank(config, companion),
		},
	}
}

func HandlerBank(config *config.Config, companion *companion.Companion) http.Handler {
	mux := http.NewServeMux()

	handler := handlers.Create(config, companion)

	mux.HandleFunc("/add", handler.PostAdd)
	mux.HandleFunc("/transfer", handler.PostTransfer)
	mux.HandleFunc("/balance", handler.GetUserInfo)

	return mux
}

func (a *App) Run() {
	go func() {
		err := a.Server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
	fmt.Println("Server started")
}

func (a *App) Stop(ctx context.Context) {
	_ = a.Server.Shutdown(ctx)
	fmt.Println("Server stopped")
}
func handleRecover(ctx context.Context) {
	if r := recover(); r != nil {
		fmt.Printf("recovered panic from %+v", r)
	}
}

func LaunchServer(server *App) {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()
	fmt.Println("Launching server")
	defer func() {
		if r := recover(); r != nil {
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			server.Stop(ctx)
			<-ctx.Done()
			fmt.Println(r)
			log.Fatal("exit")
		}
	}()
	go func() { defer handleRecover(ctx) }()
	fmt.Println("Before server.Run")
	server.Run()
	fmt.Println("After server.Run")
	<-ctx.Done()
}

func ShutdownServer(server *App) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	server.Stop(ctx)
	<-ctx.Done()
}
