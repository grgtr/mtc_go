package companion

import (
	"context"
	"errors"
	"task3/go_models/transfer"
	"task3/go_models/user"
	"task3/go_storage/storage"
)

type Companion struct {
	Storage storage.Storage
}

func Create(storage storage.Storage) *Companion {
	return &Companion{
		Storage: storage,
	}
}

func (c *Companion) GetUser(id int) (user.User, error) {
	return c.Storage.GetUser(id)
}

func (c *Companion) Transfer(ctx context.Context, transfer transfer.Transfer) error {
	if transfer.SenderId == transfer.AcceptorId {
		return errors.New("can't transfer to yourself")
	}

	if transfer.Amount <= 0 {
		return errors.New("amount must be positive")
	}

	if transfer.SenderId == -1 {
		return errors.New("sender dosn't exist")
	}

	if transfer.AcceptorId == -1 {
		return errors.New("acceptor dosn't exist")
	}

	userSender, err := c.Storage.GetUser(transfer.SenderId)
	if err != nil {
		return errors.New("sender dosn't exist")
	}

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}
	userAcceptor, err := c.Storage.GetUser(transfer.AcceptorId)
	if err != nil {
		return errors.New("acceptor dosn't exist")
	}

	if userSender.Balance < transfer.Amount {
		return errors.New("not enough money")
	}
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}
	userAcceptor.Balance += transfer.Amount
	userSender.Balance -= transfer.Amount
	err = c.Storage.SaveUser(userSender)
	if err != nil {
		return err
	}
	err = c.Storage.SaveUser(userAcceptor)
	if err != nil {
		return err
	}
	return nil
}
