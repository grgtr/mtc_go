package db

import (
	"errors"
	"math/rand"
	"task3/go_models/user"
	"task3/go_storage/storage"
	"time"
)

var _ storage.Storage = (*UsersDB)(nil)

type UsersDB struct {
	users map[int]user.User
}

func NewDB() *UsersDB {
	users := make(map[int]user.User)
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 10; i++ {
		value := rand.Intn(100)
		users[i] = user.User{Id: i, Balance: float64(value)}
	}
	return &UsersDB{
		users: users,
	}
}

func (db *UsersDB) AddUser(user user.User) error {
	if _, ok := db.users[user.Id]; ok {
		return errors.New("user already exists")
	}
	db.users[user.Id] = user
	return nil
}

func (db *UsersDB) GetUser(id int) (user.User, error) {
	if user, ok := db.users[id]; ok {
		return user, nil
	}
	return user.User{}, errors.New("user not found")
}

func (db *UsersDB) SaveUser(user user.User) error {
	db.users[user.Id] = user
	return nil
}
