package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Book struct {
	Author string
	Title  string
	Genre  string
	ID     int
}

type Storage interface {
	AddBook(book *Book) error
	GetBook(title string) (*Book, error)
}

type SliceStorage struct {
	Books []*Book
}

func (s *SliceStorage) AddBook(book *Book) error {
	s.Books = append(s.Books, book)
	return nil
}

func (s *SliceStorage) GetBook(title string) (*Book, error) {
	for _, elem := range s.Books {
		if elem.Title == title {
			return elem, nil
		}
	}
	return nil, fmt.Errorf("Книга %d не найдена", title)
}

type MapStorage struct {
	Books map[string]*Book
}

func (m *MapStorage) AddBook(book *Book) error {
	m.Books[book.Title] = book
	return nil
}

func (m *MapStorage) GetBook(title string) (*Book, error) {
	book, ok := m.Books[title]
	if !ok {
		return nil, fmt.Errorf("Книга  %d не найдена", title)
	}
	return book, nil
}

type Library struct {
	Storage    Storage
	TableSheet map[string]int
	GenerateId func(string) int
}

func (l *Library) AddBook(book *Book) error {
	book.ID = l.GenerateId(book.Title)
	l.TableSheet[book.Title] = book.ID
	err := l.Storage.AddBook(book)
	return err
}

func (l *Library) GetBookByName(name string) (*Book, error) {
	_, ok := l.TableSheet[name]
	if !ok {
		return &Book{}, fmt.Errorf("Книга с названием %s не найдена", name)
	}
	return l.Storage.GetBook(name)
}

func RandomTimeId(str string) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(1000)
}

func HashId(str string) int {
	const M = 74975671
	const lambda = 131
	el := 0
	for _, s := range str {
		el = (el*lambda + int(s)) % M
	}
	return el
}

func main() {
	sliceStorage := &SliceStorage{Books: []*Book{}}
	libraryWithSlice := Library{Storage: sliceStorage, TableSheet: make(map[string]int), GenerateId: HashId}

	book1 := Book{Author: "Лев Толстой", Title: "Анна Каренина", Genre: "Роман"}
	book2 := Book{Author: "Михаил Булгаков", Title: "Мастер и Маргарита", Genre: "Фантастика"}
	book3 := Book{Author: "Эрих Мария Ремарк", Title: "Три товарища", Genre: "Роман"}
	book4 := Book{Author: "Лев Толстой", Title: "Война и мир", Genre: "Историческая проза"}
	//book5 := Book{Author: "Антуан де Сент-Экзюпери", Title: "Маленький принц", Genre: "Психологическая проза"}

	err := libraryWithSlice.AddBook(&book1)
	if err != nil {
		fmt.Println(err)
	}
	err = libraryWithSlice.AddBook(&book2)
	if err != nil {
		fmt.Println(err)
	}

	resultBook1, err1 := libraryWithSlice.GetBookByName("Анна Каренина")
	if err1 != nil {
		fmt.Println(err1)
	} else {
		fmt.Println("Книга1:", resultBook1)
	}
	resultBook2, err2 := libraryWithSlice.GetBookByName("Мастер и Маргарита")
	if err2 != nil {
		fmt.Println(err2)
	} else {
		fmt.Println("Книга2:", resultBook2)
	}

	resultBook3, err3 := libraryWithSlice.GetBookByName("Маленький принц")
	if err3 != nil {
		fmt.Println(err3)
	} else {
		fmt.Println("Книга3:", resultBook3)
	}

	mapStorage := &MapStorage{Books: make(map[string]*Book)}
	libraryWithMap := Library{Storage: mapStorage, TableSheet: make(map[string]int), GenerateId: RandomTimeId}

	err = libraryWithMap.AddBook(&book3)
	if err != nil {
		fmt.Println(err)
	}
	err = libraryWithMap.AddBook(&book4)
	if err != nil {
		fmt.Println(err)
	}

	// Находим книги по названию в библиотеке с новым хранилищем
	resultBook3, err3 = libraryWithMap.GetBookByName("Три товарища")
	if err3 != nil {
		fmt.Println(err3)
	} else {
		fmt.Println("Книга3:", resultBook3)
	}
	resultBook4, err4 := libraryWithMap.GetBookByName("Война и мир")
	if err4 != nil {
		fmt.Println(err4)
	} else {
		fmt.Println("Книга4:", resultBook4)
	}

}
