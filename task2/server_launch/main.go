package main

import (
	"flag"
	"fmt"
	"log"
	"server/internal/config"
	"server/server_app"
)

func main() {
	var filePath string
	flag.StringVar(&filePath, "path", "server_launch/.config.json", "set config path")
	flag.Parse()

	config, err := config.Parse(filePath)
	if err != nil {
		fmt.Println(err)
		log.Fatal()
	}

	server := server_app.Create(config)
	fmt.Println("server created")
	fmt.Println("server address: ", server.Server.Addr)
	server_app.LaunchServer(server)
	fmt.Println("server launched")
	server_app.ShutdownServer(server)
	fmt.Println("server stopped")
}
