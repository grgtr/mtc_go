package server_app

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"server/internal/config"
	"server/internal/handlers"
	"syscall"
	"time"
)

type App struct {
	Config *config.Config
	Server *http.Server
}

func Create(config *config.Config) *App {
	return &App{
		Config: config,
		Server: &http.Server{
			Addr:    config.Http,
			Handler: UseHandler2(config),
		},
	}
}

func UseHandler1(config *config.Config) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("Hello, world!"))
	})
}

func UseHandler2(config *config.Config) http.Handler {
	mux := http.NewServeMux()
	handler := handlers.Handlers{
		Config: config,
	}
	mux.HandleFunc("/version", handler.GetVersion)
	mux.HandleFunc("/decode", handler.PostDecode)
	mux.HandleFunc("/hard-op", handler.GetHardOp)

	return mux
}

func (a *App) Run() {
	go func() {
		err := a.Server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
	fmt.Println("Server started")
}

func (a *App) Stop(ctx context.Context) {
	_ = a.Server.Shutdown(ctx)
	fmt.Println("Server stopped")
}

func LaunchServer(server *App) {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()
	fmt.Println("Launching server")
	defer func() {
		if v := recover(); v != nil {
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			server.Stop(ctx)
			<-ctx.Done()
			fmt.Println(v)
			os.Exit(1)
		}
	}()
	fmt.Println("Before server.Run")
	server.Run()
	fmt.Println("After server.Run")

	<-ctx.Done()
}

func ShutdownServer(server *App) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	server.Stop(ctx)
	<-ctx.Done()
}
