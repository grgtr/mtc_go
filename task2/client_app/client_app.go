package client_app

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"server/internal/config"
	"time"
)

type App struct {
	Config *config.Config
}

type Flow struct {
	Flow string `json:"flow"`
}

func Create(Config *config.Config) *App {
	return &App{
		Config: Config,
	}
}

func (c *App) GetVersion() (string, error) {
	urlAddress := url.URL{Scheme: "http", Host: c.Config.Http, Path: "/version"}
	result, err := http.Get(urlAddress.String())
	if err != nil {
		fmt.Println("err1")
		return "", err
	}

	body, err := io.ReadAll(result.Body)
	if err != nil {
		fmt.Println("err2")
		return "", err
	}

	defer result.Body.Close()
	fmt.Println("string(body)")
	return string(body), nil
}

func (c *App) Decode(str string) (string, error) {
	urlAddress := url.URL{Scheme: "http", Host: c.Config.Http, Path: "/decode"}
	result, err := json.Marshal(Flow{str})
	if err != nil {
		return "", err
	}
	response, err := http.Post(urlAddress.String(), "application/json", bytes.NewBuffer(result))
	if err != nil {
		return "", err
	}
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	flow := Flow{}
	err = json.Unmarshal(body, &flow)
	if err != nil {
		return "", err
	}

	return flow.Flow, nil
}

func (c *App) GetHardOp(ctx context.Context, timeout time.Duration) (int, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	urlAddress := url.URL{Scheme: "http", Host: c.Config.Http, Path: "/hard-op"}

	respCh := make(chan *http.Response)
	errCh := make(chan error)

	go func() {
		resp, err := http.Get(urlAddress.String())
		if err != nil {
			errCh <- err
			return
		}
		errCh <- nil
		respCh <- resp
	}()

	select {
	case err := <-errCh:
		if err != nil {
			return 0, err
		}
	case <-ctx.Done():
		return 0, fmt.Errorf("timeout")
	}

	return (<-respCh).StatusCode, nil
}
