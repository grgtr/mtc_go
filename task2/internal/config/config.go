package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	IP      string `json:"ip"`
	Port    string `json:"port"`
	Version string `json:"version"`
	Http    string `json:"ip+port"`
}

func Parse(filePath string) (*Config, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var config Config
	err = json.Unmarshal(data, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
