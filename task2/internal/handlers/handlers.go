package handlers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"server/internal/config"
	"time"
)

type Handlers struct {
	Config *config.Config
}

type Flow struct {
	Flow string `json:"flow"`
}

func (h *Handlers) GetVersion(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("1.0.0"))
	w.WriteHeader(http.StatusOK)
}

func (h *Handlers) PostDecode(w http.ResponseWriter, r *http.Request) {
	fmt.Println("in PostDecode")
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	fmt.Println("PostDecode1")
	defer func() {
		err := r.Body.Close()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}()
	fmt.Println("PostDecode2")

	var input Flow
	err = json.Unmarshal(body, &input)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Println("PostDecode3")
	outputString, err := base64.StdEncoding.DecodeString(input.Flow)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Println("PostDecode4")
	result, err := json.Marshal(Flow{string(outputString)})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(result)
	fmt.Println("out PostDecode")
}

func (h *Handlers) GetHardOp(w http.ResponseWriter, r *http.Request) {
	timeSleep := time.Duration(rand.Int()%10+1) * time.Second
	//w.WriteHeader(http.StatusOK)
	time.Sleep(timeSleep)
	if rand.Int()%3 == 0 {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
