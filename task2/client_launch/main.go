package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"server/client_app"
	"server/internal/config"
	"time"
)

func main() {
	var filePath string
	flag.StringVar(&filePath, "path", "client_launch/.config.json", "set config path")
	flag.Parse()

	config, err := config.Parse(filePath)
	if err != nil {
		fmt.Println(err)
		log.Fatal()
	}

	client := client_app.Create(config)
	fmt.Println("client created")
	ctx := context.Background()

	fmt.Println("client get version")
	version, err := client.GetVersion()
	if err != nil {
		fmt.Println("err:")
		fmt.Println(err)
	} else {
		fmt.Println("version:")
		fmt.Println(version)
	}

	fmt.Println("client decode")
	str := "SGVsbG8gd29ybGQ="
	decoded, err := client.Decode(str)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(decoded)
	}

	fmt.Println("client hard op")
	code, err := client.GetHardOp(ctx, 15*time.Second)
	if err != nil {
		fmt.Println(false, err)
	} else {
		fmt.Println(true, code)
	}
}
